package com.ruoyi.business.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.domain.BusinessFacility;
import com.ruoyi.business.service.IBusinessFacilityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import java.util.Arrays;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 在役设备使用信息管理Controller
 * 
 * @author 电力
 * @date 2024-03-29
 */
@RestController
@RequestMapping("/business/facility")
public class BusinessFacilityController extends BaseController
{
    @Autowired
    private IBusinessFacilityService businessFacilityService;

    /**
     * 查询在役设备使用信息管理列表
     */
    @PreAuthorize("@ss.hasPermi('business:facility:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusinessFacility businessFacility)
    {
        startPage();
        LambdaQueryWrapper<BusinessFacility> eq = new LambdaQueryWrapper<BusinessFacility>().eq(BusinessFacility::getFacilityStatus, businessFacility.getFacilityStatus());
        eq.like(StringUtils.isNotEmpty(businessFacility.getFacilityName()),BusinessFacility::getFacilityName,businessFacility.getFacilityName());
        List<BusinessFacility> list = businessFacilityService.list(eq);
        return getDataTable(list);
    }

    /**
     * 导出在役设备使用信息管理列表
     */
    @PreAuthorize("@ss.hasPermi('business:facility:export')")
    @Log(title = "在役设备使用信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusinessFacility businessFacility)
    {
        List<BusinessFacility> list = businessFacilityService.list();
        ExcelUtil<BusinessFacility> util = new ExcelUtil<BusinessFacility>(BusinessFacility.class);
        util.exportExcel(response, list, "在役设备使用信息管理数据");
    }

    /**
     * 获取在役设备使用信息管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:facility:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(businessFacilityService.getById(id));
    }

    /**
     * 新增在役设备使用信息管理
     */
    @PreAuthorize("@ss.hasPermi('business:facility:add')")
    @Log(title = "在役设备使用信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusinessFacility businessFacility)
    {
        return toAjax(businessFacilityService.save(businessFacility));
    }

    /**
     * 修改在役设备使用信息管理
     */
    @PreAuthorize("@ss.hasPermi('business:facility:edit')")
    @Log(title = "在役设备使用信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusinessFacility businessFacility)
    {
        return toAjax(businessFacilityService.updateById(businessFacility));
    }

    /**
     * 删除在役设备使用信息管理
     */
    @PreAuthorize("@ss.hasPermi('business:facility:remove')")
    @Log(title = "在役设备使用信息管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(businessFacilityService.removeByIds(Arrays.asList(ids)));
    }
}
