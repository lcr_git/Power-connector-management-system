package com.ruoyi.business.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BusinessFacilityMapper;
import com.ruoyi.business.domain.BusinessFacility;
import com.ruoyi.business.service.IBusinessFacilityService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

/**
 * 在役设备使用信息管理Service业务层处理
 *
 * @author 电力
 * @date 2024-03-29
 */
@Service
public class BusinessFacilityServiceImpl extends ServiceImpl<BusinessFacilityMapper, BusinessFacility> implements IBusinessFacilityService {
    @Autowired
    private BusinessFacilityMapper businessFacilityMapper;


    @Override
    @Transactional
    public boolean updateById(BusinessFacility entity) {
        // 如果出库 则设置首次使用时间
        if (entity.getFacilityStatus().equals("1")) {
            entity.setBeginTime(new Date());
            // 入库
        } else if (entity.getFacilityStatus().equals("0")) {
            // 预警 增加使用次数
        } else if (entity.getFacilityStatus().equals("2")) {
            entity.setReconditionNum(entity.getReconditionNum() + 1);
        }
        return baseMapper.updateById(entity)>0;
    }
}
