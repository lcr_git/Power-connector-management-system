package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.BusinessFacility;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * 在役设备使用信息管理Service接口
 * 
 * @author 电力
 * @date 2024-03-29
 */
public interface IBusinessFacilityService extends IService<BusinessFacility>
{

}
