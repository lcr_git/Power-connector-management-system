import request from '@/utils/request'

// 查询在途设备列表
export function listOnlineFacility(query) {
  return request({
    url: '/business/facility/list',
    method: 'get',
    params: query
  })
}

// 查询在途设备详细
export function getOnlineFacility(id) {
  return request({
    url: '/business/facility/' + id,
    method: 'get'
  })
}

// 新增在途设备
export function addOnlineFacility(data) {
  return request({
    url: '/business/facility',
    method: 'post',
    data: data
  })
}

// 修改在途设备
export function updateOnlineFacility(data) {
  return request({
    url: '/business/facility',
    method: 'put',
    data: data
  })
}

// 删除在途设备
export function delOnlineFacility(id) {
  return request({
    url: '/business/facility/' + id,
    method: 'delete'
  })
}
