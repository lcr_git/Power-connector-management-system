import request from '@/utils/request'

// 查询在役设备使用信息管理列表
export function listFacility(query) {
  return request({
    url: '/business/facility/list',
    method: 'get',
    params: query
  })
}

// 查询在役设备使用信息管理详细
export function getFacility(id) {
  return request({
    url: '/business/facility/' + id,
    method: 'get'
  })
}

// 新增在役设备使用信息管理
export function addFacility(data) {
  return request({
    url: '/business/facility',
    method: 'post',
    data: data
  })
}

// 修改在役设备使用信息管理
export function updateFacility(data) {
  return request({
    url: '/business/facility',
    method: 'put',
    data: data
  })
}

// 删除在役设备使用信息管理
export function delFacility(id) {
  return request({
    url: '/business/facility/' + id,
    method: 'delete'
  })
}
