import request from '@/utils/request'

// 查询即将退役设备列表
export function listRetirementFacility(query) {
  return request({
    url: '/business/facility/list',
    method: 'get',
    params: query
  })
}

// 查询即将退役设备详细
export function getRetirementFacility(id) {
  return request({
    url: '/business/facility/' + id,
    method: 'get'
  })
}

// 新增即将退役设备
export function addRetirementFacility(data) {
  return request({
    url: '/business/facility',
    method: 'post',
    data: data
  })
}

// 修改即将退役设备
export function updateRetirementFacility(data) {
  return request({
    url: '/business/facility',
    method: 'put',
    data: data
  })
}

// 删除即将退役设备
export function delRetirementFacility(id) {
  return request({
    url: '/business/facility/' + id,
    method: 'delete'
  })
}
