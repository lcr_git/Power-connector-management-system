package com.ruoyi.business.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 即将退役设备对象 business_facility
 * 
 * @author 电力
 * @date 2024-03-29
 */
@Data
public class BusinessFacility extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String facilityName;

    /** 年限 */
    @Excel(name = "年限")
    private String yearNumber;

    /** 行车区间 */
    @Excel(name = "行车区间")
    private String section;

    /** 具体位置 */
    @Excel(name = "具体位置")
    private String location;

    /** 剩余寿命 */
    @Excel(name = "剩余寿命")
    private String residualLife;

    /** 首次使用时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "首次使用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 检修次数 */
    @Excel(name = "检修次数")
    private String reconditionNum;

    /** 设备状态（0待出库 1在途 2预警） */
    private String facilityStatus;

    /** 是否删除 */
    private Long deleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFacilityName(String facilityName) 
    {
        this.facilityName = facilityName;
    }

    public String getFacilityName() 
    {
        return facilityName;
    }
    public void setYearNumber(String yearNumber) 
    {
        this.yearNumber = yearNumber;
    }

    public String getYearNumber() 
    {
        return yearNumber;
    }
    public void setSection(String section) 
    {
        this.section = section;
    }

    public String getSection() 
    {
        return section;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }

    public String getLocation() 
    {
        return location;
    }
    public void setResidualLife(String residualLife) 
    {
        this.residualLife = residualLife;
    }

    public String getResidualLife() 
    {
        return residualLife;
    }
    public void setBeginTime(Date beginTime) 
    {
        this.beginTime = beginTime;
    }

    public Date getBeginTime() 
    {
        return beginTime;
    }
    public void setReconditionNum(String reconditionNum) 
    {
        this.reconditionNum = reconditionNum;
    }

    public String getReconditionNum() 
    {
        return reconditionNum;
    }
    public void setFacilityStatus(String facilityStatus) 
    {
        this.facilityStatus = facilityStatus;
    }

    public String getFacilityStatus() 
    {
        return facilityStatus;
    }
    public void setDeleted(Long deleted) 
    {
        this.deleted = deleted;
    }

    public Long getDeleted() 
    {
        return deleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("facilityName", getFacilityName())
            .append("yearNumber", getYearNumber())
            .append("section", getSection())
            .append("location", getLocation())
            .append("residualLife", getResidualLife())
            .append("beginTime", getBeginTime())
            .append("reconditionNum", getReconditionNum())
            .append("facilityStatus", getFacilityStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .append("remark", getRemark())
            .toString();
    }
}
