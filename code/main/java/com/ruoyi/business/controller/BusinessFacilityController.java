package com.ruoyi.business.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.domain.BusinessFacility;
import com.ruoyi.business.service.IBusinessFacilityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import java.util.Arrays;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 即将退役设备Controller
 * 
 * @author 电力
 * @date 2024-03-29
 */
@RestController
@RequestMapping("/business/retirementFacility")
public class BusinessFacilityController extends BaseController
{
    @Autowired
    private IBusinessFacilityService businessFacilityService;

    /**
     * 查询即将退役设备列表
     */
    @PreAuthorize("@ss.hasPermi('business:retirementFacility:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusinessFacility businessFacility)
    {
        startPage();
        List<BusinessFacility> list = businessFacilityService.list();
        return getDataTable(list);
    }

    /**
     * 导出即将退役设备列表
     */
    @PreAuthorize("@ss.hasPermi('business:retirementFacility:export')")
    @Log(title = "即将退役设备", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusinessFacility businessFacility)
    {
        List<BusinessFacility> list = businessFacilityService.list();
        ExcelUtil<BusinessFacility> util = new ExcelUtil<BusinessFacility>(BusinessFacility.class);
        util.exportExcel(response, list, "即将退役设备数据");
    }

    /**
     * 获取即将退役设备详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:retirementFacility:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(businessFacilityService.getById(id));
    }

    /**
     * 新增即将退役设备
     */
    @PreAuthorize("@ss.hasPermi('business:retirementFacility:add')")
    @Log(title = "即将退役设备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusinessFacility businessFacility)
    {
        return toAjax(businessFacilityService.save(businessFacility));
    }

    /**
     * 修改即将退役设备
     */
    @PreAuthorize("@ss.hasPermi('business:retirementFacility:edit')")
    @Log(title = "即将退役设备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusinessFacility businessFacility)
    {
        return toAjax(businessFacilityService.updateById(businessFacility));
    }

    /**
     * 删除即将退役设备
     */
    @PreAuthorize("@ss.hasPermi('business:retirementFacility:remove')")
    @Log(title = "即将退役设备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(businessFacilityService.removeByIds(Arrays.asList(ids)));
    }
}
