package com.ruoyi.business.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.BusinessFacility;

/**
 * 即将退役设备Mapper接口
 * 
 * @author 电力
 * @date 2024-03-29
 */
public interface BusinessFacilityMapper  extends BaseMapper<BusinessFacility>
{

}
