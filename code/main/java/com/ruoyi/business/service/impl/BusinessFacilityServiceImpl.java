package com.ruoyi.business.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.business.mapper.BusinessFacilityMapper;
import com.ruoyi.business.domain.BusinessFacility;
import com.ruoyi.business.service.IBusinessFacilityService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 即将退役设备Service业务层处理
 * 
 * @author 电力
 * @date 2024-03-29
 */
@Service
public class BusinessFacilityServiceImpl  extends ServiceImpl<BusinessFacilityMapper, BusinessFacility> implements IBusinessFacilityService
{
    @Autowired
    private BusinessFacilityMapper businessFacilityMapper;


}
